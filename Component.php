<?php

namespace xtetis\xrbac;

class Component extends \xtetis\xengine\models\Component
{
    public static function runRbacPage()
    {

        $model_user = \xtetis\xuser\Component::isLoggedIn();

        if ($model_user)
        {
            $id_user = $model_user->id;
        }
        else
        {
            $id_user = 0;
        }

        $params = [
            'component' => \xtetis\xengine\App::getApp()->component_name,
            'action'    => \xtetis\xengine\App::getApp()->action_name,
            'query'     => \xtetis\xengine\App::getApp()->query_name,
            'id_user'   => $id_user,
        ];
        $model_rbac = new \xtetis\xrbac\models\RbacModel($params);
        if (!$model_rbac->runRbac())
        {
            \xtetis\xengine\helpers\LogHelper::customDie($model_rbac->getLastErrorMessage());
        }
    }

}
