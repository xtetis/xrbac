<?php

namespace xtetis\xrbac\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не разрешен просмотр');
}

class RbacResourceModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = 'xrbac_resource';

    /**
     * Имя компонента
     */
    public $component = '';

    /**
     * ACTION
     */
    public $action = '';

    /**
     * QUERY
     */
    public $query = '';

    

}
