<?php

namespace xtetis\xrbac\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class SqlModel
{

    /**
     * Разрешено ли пользователю доступ к ресурсу
     */
    public static function rbacIsAllowAccess(
        $id_user = 0,
        $component = '',
        $action = '',
        $query = ''
    )
    {
        $id_user   = intval($id_user);
        $component = strval($component);
        $action    = strval($action);
        $query     = strval($query);


        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xrbac_is_allow_access(:id_user,:component,:action,:query,@result,@result_str)');

        $stmt->bindParam('id_user', $id_user, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('component', $component, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('action', $action, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('query', $query, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt = $connect->prepare('SELECT @result as result, @result_str as result_str; ');

        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result']     = $row['result'];
        $ret['result_str'] = $row['result_str'];
/*
        print_r($id_user); echo "\n";
        print_r($component);echo "\n";
        print_r($action);echo "\n";
        print_r($query);echo "\n";
        print_r($ret);echo "\n";
        exit;
*/

        return $ret;
    }
}
