<?php

namespace xtetis\xrbac\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не разрешен просмотр');
}

class RbacModel extends \xtetis\xengine\models\Model
{
    /**
     * @var string
     */
    public $component = '';
    /**
     * @var string
     */
    public $action = '';
    /**
     * @var string
     */
    public $query = '';
    /**
     * @var int
     */
    public $id_user = 0;

    /**
     * @var array
     */
    public $xrbac_is_allow_access_result = [];

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        if ($this->getErrors())
        {
            return false;
        }

        $allow_create_params = [
            'component',
            'action',
            'query',
            'id_user',
        ];

        foreach ($allow_create_params as $allow_create_params_item)
        {
            if (
                (isset($params[$allow_create_params_item])) &&
                (property_exists($this, $allow_create_params_item))
            )
            {
                $this->$allow_create_params_item = $params[$allow_create_params_item];
            }
        }

    }

    /**
     * Проверяем доступ пользователя к ресурсу
     */
    public function runRbac()
    {
        if ($this->getErrors())
        {
            return false;
        }


        $this->component = strval($this->component);
        $this->component = strval($this->component);
        $this->action    = strval($this->action);
        $this->id_user     = intval($this->id_user);

        if (!strlen($this->component))
        {
            $this->addError('component', __FUNCTION__ . ': не указан параметр component');

            return false;
        }

        if (!strlen($this->action))
        {
            $this->addError('action', __FUNCTION__ . ': не указан параметр action');

            return false;
        }

        if (!strlen($this->query))
        {
            $this->addError('query', __FUNCTION__ . ': не указан параметр query');

            return false;
        }

        $this->xrbac_is_allow_access_result = \xtetis\xrbac\models\SqlModel::rbacIsAllowAccess(
            $this->id_user,
            $this->component,
            $this->action,
            $this->query
        );

        if ($this->xrbac_is_allow_access_result['result'] < 0)
        {
            $this->addError('xrbac_is_allow_access_result', 'RBAC: ' . $this->xrbac_is_allow_access_result['result_str']);

            return false;
        }

        return true;
    }

}
